## Boost proxy repository for hell

This is proxy repository for [asio library](http://think-async.com/Asio/), which allow you to build and install them using [hell dependency manager](https://gitlab.com/rilis/hell/hell).

* If you have problem with installation of boost using hell, have improvement idea, or want to request support for other versions of asio, then please [create issue here](https://gitlab.com/rilis/rilis/issues).
* If you found bug in asio itself please raport it at [asio library issues page](https://github.com/chriskohlhoff/asio/issues), because here we don't do any kind of asio library development.

## Important Notes

This proxy repo provide standalone version of Asio library. If you preffer version integrated into boost, use [boost hell-gate](https://gitlab.com/rilis/hell/gates/boost) instead.